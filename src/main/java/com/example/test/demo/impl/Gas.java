package com.example.test.demo.impl;

import com.example.test.demo.api.Engine;

@com.example.test.demo.customAnnotation.Gas
public class Gas implements Engine {

    @Override
    public void startEngine() {
        System.err.println("Uruchomiłem silnik na gaz");
    }
}
