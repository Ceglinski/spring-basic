package com.example.test.demo.impl;

import com.example.test.demo.api.Engine;

@com.example.test.demo.customAnnotation.Diesel
public class Diesel implements Engine {


    @Override
    public void startEngine() {
        System.err.println("Uruchomiłem silnik diesla!");

    }
}
